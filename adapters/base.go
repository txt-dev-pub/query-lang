package adapters

import (
	"gitlab.com/txt-dev-pub/query-lang/models"
)

type BaseAdapter[T any, O models.IAdapterOptions] interface {
	Find(params *models.AdapterParams[models.AdapterQuery]) (*models.Paginated[T], error)
	Get(id string, params *models.AdapterParams[models.AdapterQuery]) (*T, error)
	Create(data T, params *models.AdapterParams[models.AdapterQuery]) (*T, error)
	CreateMany(data []T, params *models.AdapterParams[models.AdapterQuery]) (*[]T, error)
	Update(id string, data T, params *models.AdapterParams[models.AdapterQuery]) error
	Patch(id string, data []byte, params *models.AdapterParams[models.AdapterQuery]) error
	Remove(id string, params *models.AdapterParams[models.AdapterQuery]) error
}
