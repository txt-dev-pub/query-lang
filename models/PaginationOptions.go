package models

type PaginationOptions struct {
	Initial *int64
	Max     *int64
}
