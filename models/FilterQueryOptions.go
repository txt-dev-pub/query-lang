package models

type IFilterQueryOptions interface {
	GetFilters() *FilterSettings
}

type FilterQueryOptions struct {
	Filters   *FilterSettings
	Operators *[]string
	Paginate  *PaginationParams
}
