package models

type IAdapterParams[Q AdapterQuery] interface {
	GetParams() AdapterParams[Q]
}

type AdapterParams[Q AdapterQuery] struct {
	Params[Q]
	Adapter  *AdapterOptions
	Paginate *PaginationOptions
}
