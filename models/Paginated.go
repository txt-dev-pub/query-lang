package models

type Paginated[T any] struct {
	Total int64
	Limit int64
	Skip  int64
	Data  []T
}
