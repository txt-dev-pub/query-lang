package models

type IAdapterOptions interface {
	GetId() string
	GetExtra() map[string]any
	GetOptions() *AdapterOptions
}

type AdapterOptions struct {
	Id        string
	Events    []string
	Paginate  *PaginationParams
	Multi     bool
	Filters   *FilterSettings
	Operators *[]string
	Extra     map[string]any
}

func (a AdapterOptions) GetId() string {
	return a.Id
}

func (a AdapterOptions) GetExtra() map[string]any {
	return a.Extra
}

func (a AdapterOptions) GetOptions() AdapterOptions {
	return a
}
