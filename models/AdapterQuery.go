package models

type AdapterQuery struct {
	Query
	Limit  *int64
	Skip   *int64
	Select *[]string
	Sort   *map[string]int64
}
