package models

type Params[Q AdapterQuery] struct {
	Query    *Q
	Provider *string
	Route    *map[string]any
	Headers  *map[string]any
}
