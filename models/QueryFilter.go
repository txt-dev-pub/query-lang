package models

type QueryFilter struct {
	Filters map[string]any
	Query   Query
}
